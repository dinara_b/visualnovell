﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class StateController : MonoBehaviour
{
    public VideoPlayer backgroundVideo;
    public Text storyText;
    public Text actionText;

    enum StoryState { S1, S2, S3 }
    StoryState state;

    // Start is called before the first frame update
    void Start()
    {
        state = StoryState.S1;
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case StoryState.S1: S1Action(); break;
            case StoryState.S2: S2Action(); break;
            case StoryState.S3: S3Action(); break;
        }
    }

    void SetTextPosition(Text element, float x, float y)
    {
        var rect = element.GetComponent<RectTransform>();
        rect.anchoredPosition = new Vector3(x, y, 0);
    }

    void SetTextSize(Text element, float width, float height)
    {
        var rect = element.GetComponent<RectTransform>();
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, height);
    }


    void S1Action()
    {

        backgroundVideo.source = VideoSource.VideoClip;
        backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/1");

        storyText.text = @"В июне 1935 года я приехал в Англию. Для меня это были непростые времена так как я страдал от мировой депрессии, но я будто чувствовал что у меня осталось здесь одно незаконченное дело. Сегодня мой друг, я поведаю тебе одно из своих самых интересных и таинственных расследований, надеюсь моя история которую сегодня перепишешь ты, будет иметь счастливый конец.";
        SetTextPosition(storyText, -97.3f, -126f);
        SetTextSize(storyText, 530f, 80f);

        actionText.text = "Press 'Space' to continue";
        SetTextPosition(actionText, -97.3f, -196f);
        SetTextSize(actionText, 530f, 30f);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            state = StoryState.S2;
        }
    }

    void S2Action()
    {
        backgroundVideo.source = VideoSource.VideoClip;
        backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/2");

         storyText.text = @"На днях я получил одно очень странное письмо. Оно было напечатано на машинке на плотной бумаге для записок:";
         SetTextPosition(storyText, 200.3f, -100f);
         SetTextSize(storyText, 190f, 90f);


         actionText.text = "Press 'Space' to optravitza v Andover";
         SetTextPosition(actionText, -97.3f, -196f);
         SetTextSize(actionText, 530f, 30f);

          if (Input.GetKeyDown(KeyCode.Space))
        {
            state = StoryState.S3;
        }
    }

    void S3Action()
    {
    	  backgroundVideo.source = VideoSource.VideoClip;
    	  backgroundVideo.clip = Resources.Load<VideoClip>("Backgrounds/3");

    	  storyText.text = @"Ну вот мы и на месте, теперь решай ты:";
    	  SetTextPosition(storyText, -97.3f, -126f);
          SetTextSize(storyText, 530f, 80f);

    	  actionText.text = "Press 'Space' to speak with sudmedexpert";
          SetTextPosition(actionText, -97.3f, -196f);
          SetTextSize(actionText, 530f, 30f);

    }
}
